const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String,
    },
    type: [
        {
            type: mongoose.Types.ObjectId,
            ref: "productType",
            required: true,
        }
    ],
    imageUrl: {
        type: String,
        required: true
    },

    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("product", productSchema)