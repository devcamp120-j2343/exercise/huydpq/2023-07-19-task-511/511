const mongoose = require("mongoose")

const schema = mongoose.Schema;

const productTypeSchema = new schema({
    _id: mongoose.Types.ObjectId,
	name: {
       type: String, 
       unique: true,  
       require: true
    },
	description: {
       type: String
    }

})

module.exports = mongoose.model("productType", productTypeSchema)