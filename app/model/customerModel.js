// Khai báo thư viện mongo
const mongoose = require("mongoose")

const Schema = mongoose.Schema

const customerSchema = new Schema({
    _id: mongoose.Types.ObjectId,

    fullName: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        unique: true,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        default: "",
    },
    city: {
        type: String,
        default: "",
    },
    country: {
        type: String,
        default: "",
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "order",
        }
    ],
    userID: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user",
        }
    ]
})

module.exports = mongoose.model("customer", customerSchema);