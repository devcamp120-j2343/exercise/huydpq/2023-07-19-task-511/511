const mongoose = require("mongoose");

mongoose.Promis = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./userModel");
db.role = require("./rolesModel");

db.ROLES = ["user", "admin"];

module.exports = db;