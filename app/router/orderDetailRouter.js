const express = require("express");
const { getAllOrderDetail, createOrderDetail, getOrderDetailById, updateOrderDetail, deleteOrderDetail } = require("../controller/orderDetail.controller");
const { verifyToken } = require("../middlewares/user.middeware");

const router = express.Router();

// create order detail
router.post("/orderDetail/",verifyToken, createOrderDetail);
// get all order detail
router.get("/orderDetail", getAllOrderDetail)
// get order detail by id
router.get("/orderDetail/:orderDetailId", getOrderDetailById);
// update order detail
router.put("/orderDetail/:orderDetailId", updateOrderDetail);
// delete order detail
router.delete("/orderDetail/:orderDetailId", deleteOrderDetail);

module.exports = router;