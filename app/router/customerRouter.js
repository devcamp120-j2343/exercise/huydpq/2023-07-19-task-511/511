const express = require("express");
const { createCustomer, getAllCustomer, getCustomerById, updateCustomer, deleteCustomer, getUserInfo } = require("../controller/customerController");
const { verifyToken } = require("../middlewares/user.middeware");

const router = express.Router();

// Create Customer
router.post("/customer" ,createCustomer)

// // Get All Customer
router.get("/customer" , getAllCustomer)

// // Get userInfo
router.get("/userInfo/:userId" , getUserInfo)

// // Get Customer By ID
router.get("/customer/:customerId", getCustomerById)

// // Update Customer 
router.put("/customer/:customerId", updateCustomer)

// // Delete Customer 
router.delete("/customer/:customerId", deleteCustomer)

module.exports = router