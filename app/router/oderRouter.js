const express = require("express");
const { createOrder, getAllOrder, getOrderById, updateOrder, deleteOrder } = require("../controller/orderController");

const router = express.Router();

// create order
router.post("/order", createOrder);
// get all order
router.get("/order", getAllOrder)
// get order by ID
router.get("/order/:orderId", getOrderById);
// update order
router.put("/order/:orderId", updateOrder);
// delete order
router.delete("/order/:orderId", deleteOrder);

module.exports = router;