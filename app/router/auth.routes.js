const express = require("express");
const { checkDuplicateUsername } = require("../middlewares/auth.middeware");
const { signUp, logIn } = require("../controller/authController");

const route = express.Router();

// API signup chỉ dành cho user 
route.post("/signup",checkDuplicateUsername , signUp);

route.post("/login", logIn);

module.exports = route;