const express = require("express");
const { createProductType, getAllProductType, getProductTypeByID, updateProductType, deleteProductType } = require("../controller/productTypeController");

const router = express.Router();

// Create ProductType
router.post("/productType", createProductType)

// Get All ProductType
router.get("/productType", getAllProductType)

// Get ProductType By ID
router.get("/productType/:productTypeId", getProductTypeByID)

// Update ProductType 
router.put("/productType/:productTypeId", updateProductType)

// Delete ProductType 
router.delete("/productType/:productTypeId", deleteProductType)

module.exports = router