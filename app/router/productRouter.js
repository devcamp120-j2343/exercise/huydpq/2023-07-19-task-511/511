const express = require("express");
const { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct } = require("../controller/productController");

const router = express.Router();

// Create Product
router.post("/product",createProduct)

// // Get All Product
router.get("/product", getAllProduct)

// // Get ProductType By ID
router.get("/product/:productId", getProductById)

// // Update ProductType 
router.put("/product/:productId", updateProduct)

// // Delete ProductType 
router.delete("/product/:productId", deleteProduct)

module.exports = router