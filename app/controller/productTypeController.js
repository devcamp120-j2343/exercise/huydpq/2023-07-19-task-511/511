
const { default: mongoose } = require("mongoose");
const productTypeModel = require("../model/productTypeModel")

// Create ProductType
const createProductType = async (req, res) => {
    let id = req.params.productId
    let body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: "name is invalid"
        })
    }
    try {
        let newProduct = {
            _id: new mongoose.Types.ObjectId(),
            name: body.name,
            description: body.description
        }
        const newCreateproductType = await productTypeModel.create(newProduct);

        return res.status(201).json({
            status: "Create Product successfully",
            productType: newCreateproductType,

        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// Get All ProductType
const getAllProductType = async (req, res) => {
    try {
        const productTypeList = await productTypeModel.find();
        if (productTypeList.length > 0) {
            return res.status(200).json({
                status: "Get all Product sucessfully",
                data: productTypeList
            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

// Get ProductType By ID
const getProductTypeByID = async (req, res) => {
    let id = req.params.productTypeId

    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const productTypeList = await productTypeModel.findById(id)
        if (productTypeList) {
            return res.status(200).json({
                status: "Get Product By Id sucessfully",
                data: productTypeList
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

// Update ProductType 

const updateProductType = async (req, res) => {
    let id = req.params.productTypeId
    let body = req.body;

    if (id === undefined ||!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    if (!body.name) {
        return res.status(400).json({
            message: "name is invalid"
        })
    }
    try {
        let newProduct = {
            name: body.name,
            description: body.description
        }
        const updateProduct = await productTypeModel.findByIdAndUpdate(id, newProduct)
        if (updateProduct) {
            return res.status(200).json({
                status: "Update ProductType Successfully",
               data: updateProduct
            })
        } else {
            return res.status(400).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// Delete ProductType 

const deleteProductType = async (req, res) => {
    let id = req.params.productTypeId


    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deleteProduct = await productTypeModel.findByIdAndDelete(id)
        if (deleteProduct) {
            return res.status(200).json({
                status: "Delete ProductType Successfully",
                deleteProduct
            })
        } else {
            return res.status(400).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

module.exports = { createProductType, getAllProductType, getProductTypeByID, updateProductType, deleteProductType }