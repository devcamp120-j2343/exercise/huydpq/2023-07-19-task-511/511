const db = require("../model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const signUp = async (req, res) => {
    const {username, password}= req.body
    try {
        const userRole = await db.role.findOne({
            name: "user"
        })
    
        const user = new db.user({
            username: username,
            password: bcrypt.hashSync(password, 8),
            roles: [
                userRole._id
            ]
        })
    
        await user.save();

        res.status(200).json({
            message: "Create user successfully"
        })
    } catch (error) {
        res.status(500).json({
            message: "Intenal server error",
            err: error.message
        })
    }
}

const logIn = async (req, res) => {
    try {
        const existUser = await db.user.findOne({
            username: req.body.username
        }).populate("roles");

        if(!existUser) {
            return res.status(404).send({
                message: "User not found"
            })
        }
        
        var passwordIsValid = bcrypt.compareSync(
            req.body.password,
            existUser.password
        )

        if(!passwordIsValid) {
            return res.status(401).json({
                message: "Invalid password"
            })
        }

        const secretKey = process.env.JWT_SECRET;
        console.log(secretKey)
        const token = jwt.sign({id: existUser._id}, secretKey, {
            algorithm: "HS256",
            allowInsecureKeySizes: true,
            expiresIn: 86400 // 1 ngày 
        });

        res.status(200).json({
            accessToken: token,
            userId: existUser._id
        })
    } catch (error) {
        res.status(500).json({
            message: "Intenal server error"
        })
    }
}


module.exports = {signUp, logIn}