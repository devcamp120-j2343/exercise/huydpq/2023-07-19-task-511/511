const { default: mongoose } = require("mongoose");
const customerModel = require("../model/customerModel");

// Create customer
const createCustomer = async (req, res) => {
    let { fullName, phone, email, address, country, city, orders, userID } = req.body
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is invalid!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is invalid!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is invalid!"
        })
    }
    try {
        let newCustomer = {
            fullName,
            phone,
            email,
            address,
            city,
            country,
            orders,
            userID
        }

        const filter = await customerModel.findOne({ phone: newCustomer.phone })
        if (!filter ) {
            const customerList = await customerModel.create({ ...newCustomer, _id: new mongoose.Types.ObjectId() })
            return res.status(201).json({
                status: "Create Customer successfully",
                data: customerList,

            })
        } else {
            if (filter.userID == userID) {
                const updateCustomer = await customerModel.findOneAndUpdate({_id: filter.id}, {
                    newCustomer ,  $push: { orders: newCustomer.orders }
                })

                return res.status(200).json({
                    status: "update Customer successfully",
                    data: updateCustomer,

                })
            } else {
                return res.status(404).json({
                    status: "The phone number is registered to another account",
                })
            }

        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

// get user userInfo

const getUserInfo = async (req, res) => {
    const { userId } = req.params
    try {
        const userInfo = await customerModel.find({ userID: userId })
        const data = userInfo.pop()
        return res.status(200).json({
            status: "Get User Info successfully",
            data: data,

        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

// Get All Customer
const getAllCustomer = async (req, res) => {
    try {
        const customerList = await customerModel.find()
        if (customerList.length > 0) {
            return res.status(200).json({
                status: "Get all Customer sucessfully",
                data: customerList
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

// Get Customer By Id
const getCustomerById = async (req, res) => {
    let id = req.params.customerId
    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const customerList = await customerModel.findById(id)
        if (customerList) {
            return res.status(200).json({
                status: "Get Product By Id sucessfully",
                data: customerList
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// update customer
const updateCustomer = async (req, res) => {
    let id = req.params.customerId
    let body = req.body;

    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!body.fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is invalid!"
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is invalid!"
        })
    }
    if (!body.email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is invalid!"
        })
    }
    try {
        let newCustomer = {
            fullName: body.fullName,
            phone: body.phone,
            email: body.email,
            address: body.address,
            city: body.city,
            country: body.country,
        }
        const customerList = await customerModel.findByIdAndUpdate(id, newCustomer)
        if (customerList) {
            return res.status(200).json({
                status: "Update Customer Successfully",
                data: customerList
            })
        } else {
            return res.status(400).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

// Delete Customer
const deleteCustomer = async (req, res) => {
    let id = req.params.customerId
    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const customerList = await customerModel.findByIdAndDelete(id)
        if (customerList) {
            return res.status(200).json({
                status: "Delete Customer sucessfully",
                data: customerList
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

module.exports = { createCustomer, getAllCustomer, getCustomerById, updateCustomer, deleteCustomer, getUserInfo }