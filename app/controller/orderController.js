const orderModel = require("../model/orderModel");
const customerModel = require("../model/customerModel");
const { default: mongoose } = require("mongoose");
const orderDetailModel = require("../model/orderDetailModel");

// Create  Order
const createOrder = async (req, res) => {
    let { orderDate, shippedDate, cost, note, orderDetails } = req.body;
    try {
        let newOrderDetail = orderDetails.map((item) => {
            return ({
                _id: new mongoose.Types.ObjectId(),
                product: item.product,
                quantity: item.quantity,
            })
        })
        const orderDetail = await orderDetailModel.create(newOrderDetail)
        let newOrder = {
            _id: new mongoose.Types.ObjectId(),
            orderDate,
            shippedDate,
            orderDetails: orderDetail,
            note,
            cost,

        }
        
        const createdOrder = await orderModel.create(newOrder);
        // const order = await orderModel.findById(createdOrder._id).populate("orderDetails")
        // .populate({
        //     path: "orderDetails",
        //     populate: {
        //         path: "product",
        //         select: "name amount promotionPrice"
        //     }
        // })
    
        return res.status(200).json({
            status: "Create order successfully",
            data: createdOrder,
            // order: order
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// get all oreder
const getAllOrder = async (req, res) => {
    let id = req.query.customerId;
    if (id !== undefined && !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        if (id === undefined) {
            const order = await orderModel.find();

            if (order.length > 0) {
                return res.status(200).json({
                    status: "Get all Order sucessfully",
                    data: order
                })
            } else {
                return res.status(404).json({
                    status: "Not found"
                })
            }
        } else {
            const orderOfCustomer = await customerModel.findById(id)
            if (orderOfCustomer) {
                return res.status(200).json({
                    status: "Get all Order Of Customer sucessfully",
                    data: orderOfCustomer.orders
                })
            } else {
                return res.status(404).json({
                    status: "Not found"
                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }

}


// get order by id
const getOrderById = async (req, res) => {
    let id = req.params.orderId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const order = await orderModel.findById(id).populate("orderDetails")
        .populate({
            path: "orderDetails",
            populate: {path: "product"},
            select: "name amount promotionPrice"
        })
        
        if (order) {
            return res.status(200).json({
                status: "Get Order By Id sucessfully",
                data: order
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// update order 
const updateOrder = async (req, res) => {
    let body = req.body;
    let id = req.params.orderId;
    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        let newOrder = {
            orderDate: body.orderDate,
            shippedDate: body.shippedDate,
            note: body.note,
            cost: body.cost,
        }
        const order = await customerModel.findByIdAndUpdate(id, newOrder)
        return res.status(200).json({
            status: "Update order successfully",
            data: order
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// delete order 
const deleteOrder = async (req, res) => {
    let id = req.params.orderId;
    let customerId = req.query.customerId

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        if (customerId !== undefined) {
            await customerModel.findByIdAndUpdate(customerId, {
                $pull: { orders: id }
            })
        }

        const deleteOrder = await orderModel.findByIdAndDelete(id)

        if (deleteOrder) {
            return res.status(200).json({
                message: `Delete Order sucessfully`,
                data: deleteOrder
            })
        } else {
            return res.status(404).json({
                message: `not found`,

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

module.exports = { createOrder, getAllOrder, getOrderById, updateOrder, deleteOrder }