const { default: mongoose } = require("mongoose");
const orderDetailModel = require("../model/orderDetailModel")
const orderModel = require("../model/orderModel")

const createOrderDetail = async (req, res) => {
    let {product, quantity} = req.body;
    // let id = req.params.orderId;
    // if (!mongoose.Types.ObjectId.isValid(id)) {
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "Id is invalid!"
    //     })
    // }
    try {
        let newOrderDetail = {
            _id: new mongoose.Types.ObjectId(),
            product,
            quantity
        }
       
        const createdOrderDetail = await orderDetailModel.create(newOrderDetail);
        // const order = await orderModel.findByIdAndUpdate(id, {
        //     $push: { orderDetails: newOrderDetail._id }
        // })
        return res.status(200).json({
            status: "Create Order Detail successfully",
            data: createdOrderDetail,
            // order: order

        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

const getAllOrderDetail = async (req, res) => {
    let id = req.query.orderId;
    if (id !== undefined && !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        if (id === undefined) {
            const orderDetail = await orderDetailModel.find();

            if (orderDetail.length > 0) {
                return res.status(200).json({
                    status: "Get all Order Detail sucessfully",
                    data: orderDetail
                })
            } else {
                return res.status(404).json({
                    status: "Not found"
                })
            }
        } else {
            const OrderDetailOfOrder = await orderModel.findById(id).populate("orderDetails");
            if (OrderDetailOfOrder) {
                return res.status(200).json({
                    status: "Get Order Detail Of Order sucessfully",
                    data: OrderDetailOfOrder
                })
            } else {
                return res.status(404).json({
                    status: "Not found"
                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
const getOrderDetailById = async (req, res) => {
    let id = req.params.orderDetailId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const orderDetail = await orderDetailModel.findById(id).populate("product")
        if(orderDetail){
            return res.status(200).json({
                status: "Get Order Detail Of Order sucessfully",
                data: orderDetail
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
const updateOrderDetail = async (req, res) => {
    let id = req.params.orderDetailId;
    let {body} = req
    console.log(body)
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        let newOrderDetail = {
            quantity: body.quantity,
        }
        const orderDetail = await orderDetailModel.findByIdAndUpdate(id,newOrderDetail)
        if(orderDetail){
            return res.status(200).json({
                status: "Get Order Detail Of Order sucessfully",
                data: orderDetail
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
const deleteOrderDetail = async (req, res) => {
    let {orderId} = req.query;
    let id = req.params.orderDetailId
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (id !== undefined &&!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is invalid!"
        })
    }
    try {
        const deleteOrderDetail = await orderDetailModel.findByIdAndDelete(id)
        if(orderId !== undefined){
           await orderModel.findByIdAndUpdate(orderId, {
            $pull: {orderDetails: id}
           })
        }  
        if(deleteOrderDetail){
            return res.status(200).json({
                status: "Get Order Detail Of Order sucessfully",
                data: deleteOrderDetail
            })
        } else {
            return res.status(404).json({
                status: "Not found"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}

module.exports = { createOrderDetail, getAllOrderDetail, getOrderDetailById, updateOrderDetail, deleteOrderDetail }