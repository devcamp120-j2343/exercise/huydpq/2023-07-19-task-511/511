const { default: mongoose } = require("mongoose");
const productModel = require("../model/productModel");
const productTypeModel = require("../model/productTypeModel")
const PAGE_SIZE = 2
// Create Product
const createProduct = async (req, res) => {
    let body = req.body;

    if (!body.name) {
        return res.status(400).json({
            message: "name is invalid"
        })
    }
    try {
        let newProduct = {
            _id: new mongoose.Types.ObjectId(),
            name: body.name,
            description: body.description,
            type: body.type,
            imageUrl: body.imageUrl,
            buyPrice: body.buyPrice,
            promotionPrice: body.promotionPrice,
            amount: body.amount,
        }

        const createProduct = await productModel.create(newProduct)

        return res.status(200).json({
            status: "Create Product successfully",
            data: createProduct
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// Get All Product
const getAllProduct = async (req, res) => {
    let page = req.query._page;
    let limit = req.query._limit;
    var brank = req.query.brank;
    var minPrice = req.query.min;
    var maxPrice = req.query.max;

    try {
        // var soLuongBoQua = (page - 1) * limit
        let condition = {}
        if (brank) {
            condition.type = brank
        }
        if (minPrice) {
            condition.promotionPrice = { $gte: minPrice }
        }
        if (maxPrice) {
            condition.promotionPrice = { ...condition.promotionPrice, $lte: maxPrice }
        }

        
        const product = await productModel.find(condition).skip(page).limit(limit)
        const totalProduct = await productModel.count(condition);
        // String, ""
        // Number, 0 NAn
        // boolean, false
        //Object, 
        // cả 4 phủ định là null undefined
        // c = a ?? b : a khác null và undefined thì c = a 
        // c = a || b : c = a ? a : b

        return res.status(200).json({
            status: "Get all Product sucessfully",
            total : totalProduct,
            data: product,
            
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// get Product By Id 
const getProductById = async (req, res) => {
    let id = req.params.productId;
    // let brank = req.query.brank;
    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const product = await productModel.findById(id)
        const productType = await productModel.find({type: product.type})
        return res.status(200).json({
            status: "Get Product By ID Successfully",
            dataType: productType,
            data: product
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// update Product
const updateProduct = async (req, res) => {
    let id = req.params.productId;
    let body = req.body

    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        let newProduct = {
            name: body.name,
            description: body.description,
            type: body.type,
            imageUrl: body.imageUrl,
            buyPrice: body.buyPrice,
            promotionPrice: body.promotionPrice,
            amount: body.amount,
        }
        const newUpdateProduct = await productModel.findByIdAndUpdate(id, newProduct)

        if (newUpdateProduct) {
            return res.status(200).json({
                status: "Update Product sucessfully",
                data: newUpdateProduct

            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }
}
// delete Product 
const deleteProduct = async (req, res) => {
    let id = req.params.productId;
    if (id === undefined || !mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    try {
        const deleteProduct = await productModel.findByIdAndDelete(id)

        if (deleteProduct) {
            return res.status(200).json({
                status: "Update Product sucessfully",
                data: deleteProduct

            })
        } else {
            return res.status(404).json({
                status: "Not found any review"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            err: error.message
        })
    }

}

module.exports = { createProduct, getAllProduct, getProductById, updateProduct, deleteProduct }