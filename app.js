const express = require("express");

const app = express();
const cros = require("cors")
const mongoose = require("mongoose");
const db = require("./app/model")
const {initial} = require("./data.js")
require ("dotenv").config()
const port = 8000;
// khai báo model
const productTypeModel= require("./app/model/productTypeModel");
const productModel = require("./app/model/productModel");
const customerModel = require("./app/model/customerModel")
const orderModel = require("./app/model/orderModel")
const orderDetailModel = require("./app/model/orderDetailModel")

// khai báo Router
const productTypeRouter = require("./app/router/productTypeRouter");
const productRouter = require("./app/router/productRouter");
const customerRouter = require("./app/router/customerRouter")
const orderRouter = require("./app/router/oderRouter")
const orderDetailRouter = require("./app/router/orderDetailRouter")
const authRoute = require ("./app/router/auth.routes")
app.use(express.json());
app.use(cros())
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h")
  .then(() => {
    console.log("Connected to Mongo Successfully");
    initial()
  })
  .catch(error => {
    console.error("Error", error);
    process.exit()
  });

app.use("/", productTypeRouter)
app.use("/", productRouter)
app.use("/",customerRouter)
app.use("/",orderRouter)
app.use("/",orderDetailRouter)
app.use("/auth",authRoute)

app.listen(port, () => {
    console.log(`Đang chạy cổng ${port}`) 
}) 